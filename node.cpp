#include "node.h"

// Take in value and create a node
node::node(int input)
{
    data = input;
    *next = nullptr;


}
// Takes in an array of values and creates the appropriate nodes
node::node(int values[], int length)
{

    for(int i = 0; i < length; i++){
        node(values[i]);
    }


}

// Default destructor
node::~node()
{
    /*do {
            delete next;
        }while (next != nullptr);*/

    // Hint: You don't want to just delete the current node. You need to keep track of what is next
}

// Add a value to the end node
void node::append(int input)
{
    node* a;
    a->data = data;
   if(next != nullptr){
        while (next != nullptr) {
            a = a->next;
        }
       a->data = input;
    }
    else
   {
       a->data = input;
   }
}

// Add an array of values to the end as separate nodes
void node::append(int inputs[], int length)
{


}

// Insert a new node after the given location
node* node::insert(int location, int value)
{
    // Must return head pointer location
}

// Remove a node and link the next node to the previous node
node* node::remove(int location)
{
    // Must return head pointer location
}

// Print all nodes
void node::print()
{

}

//Print the middle node
void node::print_middle()
{
    // HINT: Use a runner to traverse through the linked list at two different rates, 1 node per step
    //       and two nodes per step. When the faster one reaches the end, the slow one should be
    //       pointing to the middle
}

// Get the value of a given node
int node::get_value(int location)
{

}

// Overwrite the value of a given node
void node::set_data(int location, int value)
{

}